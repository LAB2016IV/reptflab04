--liquibase formatted sql

--changeset arthur:01
create table tipo(
	tip_id int(4) NOT NULL primary key auto_increment,
	tip_descricao varchar(255)
);
--rollback DROP TABLE tipo;

--changeset arthur:02
create table produto(
	pro_id int(4) NOT NULL primary key auto_increment,
	pro_valor double,
	pro_nome varchar(255)
);
--rollback DROP TABLE produto;

--changeset arthur:03
create table estoque (
	est_id int(4) NOT NULL primary key auto_increment,
	est_quant int
);
--rollback DROP TABLE estoque;

--changeset arthur:04
create table cliente(
	cli_id int(4) NOT NULL primary key auto_increment,
	cli_nome varchar(255)
);
--rollback DROP TABLE cliente;

--changeset arthur:05
create table funcionario(
	fun_matricula int(4) NOT NULL primary key auto_increment,
	fun_nome varchar(255),
	fun_privilegio int(1)
);
--rollback DROP TABLE funcionario;

--changeset arthur:06
create table fornecedor(
	for_id int(4) NOT NULL primary key auto_increment,
	for_tel varchar(11),
	for_nome varchar(255)
);
--rollback DROP TABLE fornecedor;

--changeset arthur:07
create table acarretamentos(
	aca_id int(4) NOT NULL primary key auto_increment,
	aca_ocorrido varchar(255)
);
--rollback DROP TABLE acarretamentos;

--changeset arthur:08
create table entrada(
	ent_id int(4) NOT NULL primary key auto_increment,
	ent_valor double,
	ent_data_entrada date,
	ent_qtd int
);
--rollback DROP TABLE entrada;

--changeset arthur:09
create table pagamento(
	pag_id int(4) NOT NULL primary key auto_increment,
	pag_qtd_parcelas int,
	pag_nome varchar(255)
);
--rollback DROP TABLE pagamento;

--changeset arthur:10
create table venda (
	ven_id int(4) NOT NULL primary key auto_increment,
	ven_qtd int,
	ven_data_venda date	
);
--rollback DROP TABLE venda;