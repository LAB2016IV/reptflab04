package edu.fatec.sjc.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="fornecedor", catalog="fatecsjc")
public class Fornecedor implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public Fornecedor(Integer fortelefone, String fornome)
	{
		this.fortelefone = fortelefone;
		this.fornome = fornome; 
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "for_id", unique = true, nullable = false)
	private Integer forid;
	
	@Column(name = "for_telefone", nullable = false, length = 100)
	private Integer fortelefone;
	
	@Column(name = "for_nome", nullable = false, length = 100)
	private String fornome;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "fornecedor",cascade = {CascadeType.ALL})
	@JsonManagedReference
	private Collection<Entrada> entrada = new HashSet<Entrada>();
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "fornecedor",cascade = {CascadeType.ALL})
	@JsonManagedReference
	private Collection<Acarretamentos> acarretamentos = new HashSet<Acarretamentos>();
	
	
	@JsonManagedReference
	private Estoque estoque;

	public Integer getForid() {
		return this.forid;
	}

	public Integer setForid() {
		return this.forid;
	}

}
