package edu.fatec.sjc.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="venda", catalog="fatecsjc")

public class Venda implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public Venda(Integer venqtd, Date vendata_venda, Produto produto, Funcionario funcionario, Cliente cliente, Pagamento pagamento)
	{
		this.venqtd = venqtd;
		this.vendata_venda = vendata_venda;
		this.produto = produto;
		this.funcionario = funcionario;
		this.cliente = cliente;
		this.pagamento = pagamento;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ven_id", unique = true, nullable = false, length = 4)
	private Integer venid;
	
	@Column(name = "ven_qtd", nullable = false)
	private Integer venqtd;
	
	@Column(name = "ven_data_venda", nullable = false)
	private Date vendata_venda;
	
	@ManyToOne
	private Produto produto;
	
	@ManyToOne
	private Funcionario funcionario;
	
	@ManyToOne
	private Cliente cliente;

	@ManyToOne
	private Pagamento pagamento;

}
