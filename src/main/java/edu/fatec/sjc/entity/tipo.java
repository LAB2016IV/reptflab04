package edu.fatec.sjc.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="tipo", catalog="fatecsjc")

public class Tipo implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public Tipo(String tipdescricao)
	{
		this.tipdescricao = tipdescricao;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "tip_id", unique = true, nullable = true)
	private Integer tipid;
	
	@Column(name = "tip_descricao", nullable = false, length = 255)
	private String tipdescricao;
	
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "tipo",cascade = {CascadeType.ALL})
	@JsonBackReference
	private Collection<Produto> produto = new HashSet<Produto>();
	

	public Integer gettipid()
	{
		return this.tipid;
	}
	
	public void settipid(Integer f)
	{
		this.tipid = f;
	}

	
	
}
