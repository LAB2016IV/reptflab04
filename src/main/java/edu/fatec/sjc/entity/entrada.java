package edu.fatec.sjc.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="entrada", catalog="fatecsjc")
public class Entrada implements  Serializable {
	private static final long serialVersionUID = 1L;
	
	public Entrada(Double entvalor, Date entdata_entrada, Integer entqtd, Fornecedor fornecedor, Produto produto)
	{
		this.entvalor = entvalor;
		this.entdata_entrada = entdata_entrada;
		this.entqtd = entqtd;
		this.fornecedor = fornecedor;
		this.produto = produto;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ent_id", unique = true, nullable = false)
	private Integer entid;
	
	@Column(name = "ent_valor", nullable = false, length = 100)
	private Double entvalor;
	
	@Column(name = "ent_data_entrada", nullable = false)
	private Date entdata_entrada;
	
	@Column(name = "ent_qtd", nullable = false, length = 100)
	private Integer entqtd;
	
	@ManyToOne
	private Fornecedor fornecedor;
	
	
	@JsonBackReference
	private Produto produto = new Produto(entvalor, null, null);

	public Produto getProduto() {
		return this.produto;
	}

	public void setProduto(Produto p) {
		this.produto = p;
	}

	
	
	
}
