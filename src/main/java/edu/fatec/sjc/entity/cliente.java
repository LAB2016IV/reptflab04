package edu.fatec.sjc.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="cliente",catalog="fatecsjc")
public class Cliente implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public Cliente(String clinome)
	{
		this.clinome = clinome;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cli_id", unique = true, nullable = false)
	private Integer clienteId;
	
	@Column(name = "cli_nome", nullable = false, length = 255)
	private String clinome;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "cliente",cascade = {CascadeType.ALL})
	private Collection<Venda> venda = new HashSet<Venda>();

}
