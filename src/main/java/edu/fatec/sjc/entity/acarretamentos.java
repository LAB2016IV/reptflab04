package edu.fatec.sjc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="acarretamentos", catalog="fatecsjc")

public class Acarretamentos implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public Acarretamentos(Fornecedor fornecedor, String ocorrido)
	{
		this.fornecedor = fornecedor;
		this.ocorrido = ocorrido;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "aca_id", unique = true, nullable = false)
	private Integer acaid;
	
	@Column(name = "aca_ocorrido", unique = true, nullable = false, length = 255)
	private String ocorrido;
	
	@ManyToOne
	private Fornecedor fornecedor;
	
	
	public Fornecedor getFornecedor()
	{
		return this.fornecedor;
	}
	
	public void setFornecedor(Fornecedor f)
	{
		this.fornecedor = f;
	}
	
}
