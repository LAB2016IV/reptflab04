package edu.fatec.sjc.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="produto", catalog="fatecsjc")

public class Produto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public Produto(Double provalor, String pronome, Tipo tipo)
	{
		this.provalor = provalor;
		this.pronome = pronome;
		this.tipo = tipo;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pro_id", unique = true, nullable = false)
	private Integer proid;
	
	@Column(name = "pro_valor", nullable = false)
	private Double provalor;
	
	@Column(name = "pro_nome", nullable = false, length = 255)
	private String pronome;
	
	@ManyToOne
	private Tipo tipo;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "produto",cascade = {CascadeType.ALL})
	@JsonBackReference
	private Collection<Venda> venda = new HashSet<Venda>();
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "produto",cascade = {CascadeType.ALL})
	@JsonBackReference
	private  Collection<Estoque> estoque = new HashSet<Estoque>();;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "produto",cascade = {CascadeType.ALL})
	@JsonBackReference
	private Collection<Entrada> entrada = new HashSet<Entrada>();
	
	
	public Integer getproid()
	{
		return this.proid;
	}
	
	public void setproid(Integer f)
	{
		this.proid = f;
	}


	

}
