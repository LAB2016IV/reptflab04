package edu.fatec.sjc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="estoque", catalog = "fatecsjc")
public class Estoque implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public Estoque(Integer estquant, Produto produto)
	{
		this.estquant = estquant;
		this.produto = produto;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "est_id", unique = true, nullable = false)
	private Integer estid;
	
	@Column(name = "est_quant", nullable = false)
	private Integer estquant;
	
	@JsonBackReference
	private Produto produto = new Produto(null, null, null);

	public Produto getProduto() {
		return this.produto;
	}

	public void setProduto(Produto p) {
		this.produto = p;
	}
	
	public Integer getEstid() {
		return this.estid;
	}

	public Integer setEstid() {
		return this.estid;
	}
	
	

}
