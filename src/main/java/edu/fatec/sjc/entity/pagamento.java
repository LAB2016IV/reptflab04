package edu.fatec.sjc.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="pagamento", catalog="fatecsjc")
public class Pagamento implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public Pagamento(Integer pagqtd_parcela, String pagnome)
	{
		this.pagqtd_parcela = pagqtd_parcela;
		this.pagnome = pagnome;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pag_id", unique = true, nullable = false)
	private Integer pagId;
	
	@Column(name = "pag_qtd_parcela", nullable = false, length = 100)
	private Integer pagqtd_parcela;
	
	@Column(name = "pag_nome", nullable = false, length = 100)
	private String pagnome;
	
	
	@OneToOne(fetch = FetchType.LAZY,cascade = {CascadeType.ALL})
	private Venda venda;
	
}
