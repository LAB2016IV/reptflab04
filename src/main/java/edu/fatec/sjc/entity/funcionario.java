package edu.fatec.sjc.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.NoArgsConstructor;


@Entity
@Table(name="funcionario", catalog="fatecsjc")
@NoArgsConstructor

public class Funcionario implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public Funcionario(String funnome, Integer funprivilegio){
		this.funnome = funnome;
		this.funprivilegio = funprivilegio;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "fun_matricula", nullable = false)
	private Integer funmatricula;
	
	@Column(name = "fun_nome", nullable = false, length = 255)
	private String funnome;
	
	@Column(name = "fun_privilegio", nullable = false, length = 1)
	private Integer funprivilegio;
	
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "funcionario",cascade = {CascadeType.ALL})
	private Collection<Venda> venda = new HashSet<Venda>();
}
	
