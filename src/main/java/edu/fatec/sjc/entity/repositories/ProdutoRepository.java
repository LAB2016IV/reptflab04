package edu.fatec.sjc.entity.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import edu.fatec.sjc.entity.Entrada;
import edu.fatec.sjc.entity.Produto;

@Repository
public interface ProdutoRepository extends CrudRepository<Produto, Integer> {

@Query("Select a produto a where a.Produtopro_id = :pro_id")
Produto selecionaProdutoPorpro_id(@Param("pro_id")Integer pro_id);


@Query("SELECT a FROM produto a")
List<Produto> encontrarProdutos();


@Query("Select a from tipo a where a.tipo = :tip_id")
List<Entrada> selecionarEntradasProduto(@Param("tip_id") String tipo);


}