package edu.fatec.sjc.entity.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import edu.fatec.sjc.entity.Acarretamentos;
import edu.fatec.sjc.entity.Entrada;


	@Repository
	public interface AcarretamentosRepository extends CrudRepository<Acarretamentos, Integer> {
		
		@Query("Select a acarretamento a where a.acarretamentoAca_id = :aca_id")
		Acarretamentos selecionaAcarretamentoPoraca_id(@Param("aca_id")Integer aca_id);
		
		@Query("SELECT a FROM acarretamento a")
		List<Acarretamentos> encontrarAcarretamentos();
		
		@Query("Select a from Entrada a where a.fornecedor = :for_id")
		List<Entrada> selecionarEntradasProduto(@Param("for_id") String fornecedor);
}

