package edu.fatec.sjc.entity.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import edu.fatec.sjc.entity.Venda;

@Repository
public interface VendaRepository extends CrudRepository<Venda, Integer> {

@Query("Select a venda a where a.Vendaven_id = :ven_id")
Venda selecionaVendasPorven_id(@Param("ven_id")Integer ven_id);


@Query("SELECT a FROM venda a")
List<Venda> encontrarVendas();


@Query("Select a from venda e where a.produto = :pro_id")
List<Venda> selecionaVendasProduto(@Param("pro_id") String produto);


@Query("Select a from venda a where a.funcionario = :fun_matricula")
List<Venda> selecionaVendasFuncionario(@Param("fun_matricula") String funcionario);


@Query("Select a from venda a where a.cliente = :cli_id")
List<Venda> selecionaVendasCliente(@Param("cli_id") String cliente);


@Query("Select a from venda a where a.pagamento = :pag_id")
List<Venda> selecionaVendasPagamento(@Param("pag_id") String pagamento);



}