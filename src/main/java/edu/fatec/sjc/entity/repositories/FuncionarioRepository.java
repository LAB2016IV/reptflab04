package edu.fatec.sjc.entity.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import edu.fatec.sjc.entity.Funcionario;

@Repository
public interface FuncionarioRepository extends CrudRepository<Funcionario, Integer> {

@Query("Select a funcionario a where a.Funcionariofun_matricula = :fun_matricula")
Funcionario selecionaFuncionarioPorfun_matricula(@Param("fun_matricula")Integer fun_matricula);


@Query("SELECT a FROM funcionario a")
List<Funcionario> encontrarFuncionarios();


}