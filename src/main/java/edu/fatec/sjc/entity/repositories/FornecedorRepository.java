package edu.fatec.sjc.entity.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import edu.fatec.sjc.entity.Fornecedor;

@Repository
public interface FornecedorRepository extends CrudRepository<Fornecedor, Integer> {

@Query("Select a fornecedor a where a.Fornecedorfor_id = :for_id")
Fornecedor selecionaFornecedorPorfor_id(@Param("for_id")Integer for_id);

@Query("SELECT a FROM fornecedor a")
List<Fornecedor> encontrarFornecedor();

}
