package edu.fatec.sjc.entity.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import edu.fatec.sjc.entity.Pagamento;

@Repository
public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {

@Query("Select a pagamento a where a.Pagamentopag_id = :pag_id")
Pagamento selecionaFuncionarioPorpag_id(@Param("pag_id")Integer pag_id);


@Query("SELECT a FROM pagamento a")
List<Pagamento> encontrarPagamentos();


}