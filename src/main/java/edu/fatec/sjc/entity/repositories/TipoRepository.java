package edu.fatec.sjc.entity.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import edu.fatec.sjc.entity.Tipo;

@Repository
public interface TipoRepository extends CrudRepository<Tipo, Integer> {

@Query("Select a tipo a where a.Tipotip_id = :tip_id")
Tipo selecionaTipoPortip_id(@Param("tip_id")Integer tip_id);


@Query("SELECT a FROM tipo a")
List<Tipo> encontrarTipos();


}