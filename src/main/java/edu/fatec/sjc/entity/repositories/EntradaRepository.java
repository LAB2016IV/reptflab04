package edu.fatec.sjc.entity.repositories;

import java.util.Calendar;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import edu.fatec.sjc.entity.Entrada;

@Repository
public interface EntradaRepository extends CrudRepository<Entrada, Integer> {

	@Query("Select e from entrada e where e.entradaDia = :ent_data_entrada")
	List<Entrada> selecionarEntradasDia(@Param("ent_data_entrada") Calendar ent_data_entrada);

	
	@Query("Select e entrada a where e.Entradaent_id = :ent_id")
	Entrada selecionaEntradasPorent_id(@Param("ent_id")Integer ent_id);
	
	
	@Query("SELECT e FROM entrada e")
	List<Entrada> encontrarEntradas();
	

	@Query("Select e from entrada e where e.fornecedor = :for_id")
	List<Entrada> selecionaEntradasFornecedor(@Param("for_id") String fornecedor);

	
	@Query("Select e from entrada e where e.produto = :pro_id")
	List<Entrada> selecionaEntradasProduto(@Param("pro_id") String produto);


}
