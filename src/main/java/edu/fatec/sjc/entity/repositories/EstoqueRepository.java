package edu.fatec.sjc.entity.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import edu.fatec.sjc.entity.Estoque;
import edu.fatec.sjc.entity.Produto;

@Repository
public interface EstoqueRepository extends CrudRepository<Estoque, Integer> {

@Query("Select a estoque a where a.Estoqueest_id = :est_id")
Estoque selecionaEstoquePorest_id(@Param("est_id")Integer est_id);

@Query("SELECT a FROM estoque a")
List<Estoque> encontrarEstoques();

@Query("Select e.estoqueQtd from estoque e where e.produto = :produto")
Integer quantidade(@Param("produto") Produto produto);

@Modifying
@Query("Update Estoque e set e.estoqueQtd = :estquant where e.produto = :p")
void atualizaEstoque(@Param("estquant") Integer estquant, @Param("p") Produto produto);

Estoque encontrarProduto(Produto produto);



}