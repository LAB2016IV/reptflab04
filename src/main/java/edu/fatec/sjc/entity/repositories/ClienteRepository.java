package edu.fatec.sjc.entity.repositories;

import java.util.List;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import edu.fatec.sjc.entity.Cliente;

	@Repository
	public interface ClienteRepository extends CrudRepository<Cliente, Integer> {
		
		@Query("Select a cliente a where a.clientecli_id = :cli_id")
		Cliente selecionaClientesPorcli_id(@Param("cli_id")Integer cli_id);
		
		
		@Query("SELECT a FROM Categoria a")
		List<Cliente> encontrarClientes();
}


