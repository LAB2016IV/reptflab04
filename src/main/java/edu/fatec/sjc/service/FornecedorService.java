package edu.fatec.sjc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import edu.fatec.sjc.entity.Fornecedor;
import edu.fatec.sjc.entity.repositories.FornecedorRepository;

public class FornecedorService {
	

	@Autowired
	private FornecedorRepository fornecedorRepository;
	
	@Transactional
	public void salvarFornecedor(Fornecedor fornecedor){
		
		fornecedorRepository.save(fornecedor);
	}
	
	public List<Fornecedor> listarFornecedores(){
		return fornecedorRepository.encontrarFornecedor();
	}
	
	public Fornecedor umFornecedor(Integer for_id){
		return fornecedorRepository.selecionaFornecedorPorfor_id(for_id);
	}
	

	public List<Fornecedor> listar() {
		return fornecedorRepository.encontrarFornecedor();
	}

}
