package edu.fatec.sjc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import edu.fatec.sjc.entity.Estoque;
import edu.fatec.sjc.entity.Produto;
import edu.fatec.sjc.entity.repositories.EstoqueRepository;
import edu.fatec.sjc.entity.repositories.ProdutoRepository;

public class ProdutoService {
	
	
	@Autowired
	private ProdutoRepository produtoRepository;
	@Autowired
	private EstoqueRepository estoqueRepository;

	@Transactional
	public void salvarProduto(Produto produto) {
		Estoque estoque = estoqueRepository.selecionaEstoquePorest_id(produto.getproid());
		estoque.setProduto(produto);

		produtoRepository.save(produto);
	}

	public List<Produto> listarEstoque() {
		return produtoRepository.encontrarProdutos();
	}

	public Produto umProduto(Integer pro_id) {
		return produtoRepository.selecionaProdutoPorpro_id(pro_id);
	}

	public Estoque estoqueProduto(Produto produto) {
		return estoqueRepository.encontrarProduto(produto);
	}
	
	public Integer quantidade(Produto p){
		return estoqueRepository.quantidade(p);
	}

}
