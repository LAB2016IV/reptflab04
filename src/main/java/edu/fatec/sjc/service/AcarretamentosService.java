package edu.fatec.sjc.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.fatec.sjc.entity.Acarretamentos;
import edu.fatec.sjc.entity.Entrada;
import edu.fatec.sjc.entity.Fornecedor;
import edu.fatec.sjc.entity.repositories.AcarretamentosRepository;
import edu.fatec.sjc.entity.repositories.FornecedorRepository;

@Service
public class AcarretamentosService {

	@Autowired
	private AcarretamentosRepository acarretamentosRepository;
	
	@Autowired
	private FornecedorRepository fornecedorRepository;
	
	@Transactional
	public void salvarEntradas(Acarretamentos acarretamentos) {
		Fornecedor fornecedor = fornecedorRepository.selecionaFornecedorPorfor_id(acarretamentos.getFornecedor().getForid());
		acarretamentos.setFornecedor(fornecedor);

		acarretamentosRepository.save(acarretamentos);
	}
	
	public List<Acarretamentos> listarAcarretamentos() {
		return acarretamentosRepository.encontrarAcarretamentos();
	}

	public Acarretamentos umAcarretamentos(Integer aca_id) {
		// TODO Auto-generated method stub
		return null;
	}

}
