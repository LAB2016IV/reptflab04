package edu.fatec.sjc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.fatec.sjc.entity.Cliente;
import edu.fatec.sjc.entity.repositories.ClienteRepository;

@Service
public class ClienteService {
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Transactional
	public void salvarClientes(Cliente cliente){
	
			clienteRepository.save(cliente);
	}
	
	public List<Cliente> listarClientes(){
		return clienteRepository.encontrarClientes();
	}
	
	public Cliente umCliente(Integer cli_id){
		return clienteRepository.selecionaClientesPorcli_id(cli_id);
	}

}
