package edu.fatec.sjc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import edu.fatec.sjc.entity.Estoque;
import edu.fatec.sjc.entity.Produto;
import edu.fatec.sjc.entity.repositories.EstoqueRepository;
import edu.fatec.sjc.entity.repositories.ProdutoRepository;

public class EstoqueService {
	

	@Autowired
	private EstoqueRepository estoqueRepository;
	@Autowired
	private ProdutoRepository produtoRepository;

	@Transactional
	public void salvarEstoque(Estoque estoque) {
		Produto produto = produtoRepository.selecionaProdutoPorpro_id(estoque.getProduto().getproid());
		estoque.setProduto(produto);

		estoqueRepository.save(estoque);
	}

	public List<Estoque> listarEstoque() {
		return estoqueRepository.encontrarEstoques();
	}

	public Estoque umEstoque(Integer est_id) {
		return estoqueRepository.selecionaEstoquePorest_id(est_id);
	}

	public Estoque estoqueProduto(Produto produto) {
		return (Estoque) estoqueRepository.encontrarEstoques();
	}
	
	public Integer quantidade(Produto p){
		return estoqueRepository.quantidade(p);
	}



}
