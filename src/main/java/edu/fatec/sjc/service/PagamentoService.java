package edu.fatec.sjc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import edu.fatec.sjc.entity.Pagamento;
import edu.fatec.sjc.entity.repositories.PagamentoRepository;

public class PagamentoService {
	
	@Autowired
	private PagamentoRepository pagamentoRepository;
	
	@Transactional
	public void salvarPagamento(Pagamento pagamento){
		
		pagamentoRepository.save(pagamento);
	}
	
	public List<Pagamento> listarPagamentos(){
		return pagamentoRepository.encontrarPagamentos();
	}
	
	public Pagamento umPagamento(Integer pag_id){
		return pagamentoRepository.selecionaFuncionarioPorpag_id(pag_id);
	}


}
