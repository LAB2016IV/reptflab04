package edu.fatec.sjc.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import edu.fatec.sjc.entity.Entrada;
import edu.fatec.sjc.entity.Produto;
import edu.fatec.sjc.entity.repositories.EntradaRepository;
import edu.fatec.sjc.entity.repositories.ProdutoRepository;

public class EntradaService {

	@Autowired
	private EntradaRepository entradaRepository;
	@Autowired
	private ProdutoRepository produtoRepository;


	@Transactional
	public void salvarEntradas(Entrada entrada) {
		Produto produto = produtoRepository.selecionaProdutoPorpro_id(entrada.getProduto().getproid());
		entrada.setProduto(produto);

		entradaRepository.save(entrada);
	}

	public List<Entrada> listarEntradas() {
		return entradaRepository.encontrarEntradas();
	}

	public List<Entrada> ent_data_entrada(String ent_data_entrada) throws ParseException {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
		cal.setTime(sdf.parse(ent_data_entrada));
		return entradaRepository.selecionarEntradasDia(cal);
	}

	public List<Entrada> entradasProduto(String produto) 
	{
	    return entradaRepository.selecionaEntradasProduto(produto);
	}

	public List<Entrada> entradasEnt_id(Integer ent_id) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Entrada> entradasFornecedor(String fornecedor) {
		// TODO Auto-generated method stub
		return null;
	}

	
}
