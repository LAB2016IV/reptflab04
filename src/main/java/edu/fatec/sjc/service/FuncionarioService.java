package edu.fatec.sjc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import edu.fatec.sjc.entity.Funcionario;
import edu.fatec.sjc.entity.repositories.FuncionarioRepository;

public class FuncionarioService {

	@Autowired
	private FuncionarioRepository funcionarioRepository;
	
	@Transactional
	public void salvarFuncionario(Funcionario funcionario){
		
		funcionarioRepository.save(funcionario);
	}
	
	public List<Funcionario> listarFuncionarios(){
		return funcionarioRepository.encontrarFuncionarios();
	}
	
	public Funcionario umFuncionario(Integer fun_matricula){
		return funcionarioRepository.selecionaFuncionarioPorfun_matricula(fun_matricula);
	}


}
