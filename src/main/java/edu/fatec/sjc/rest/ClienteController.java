package edu.fatec.sjc.rest;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import edu.fatec.sjc.entity.Cliente;
import edu.fatec.sjc.entity.repositories.ClienteRepository;

public class ClienteController {
	

	@Autowired
	ClienteController clienteService;

	@Autowired
	ClienteRepository clienteRepository;

	@RequestMapping(value = "/cliente/lista", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Cliente>> listar() {
		List<Cliente> lista = clienteService.listarClientes();
		if (lista.isEmpty()) {
			System.out.println("Nenhum registro no banco de dados.");
			return new ResponseEntity<List<Cliente>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Cliente>>(lista, HttpStatus.OK);
	}

	private List<Cliente> listarClientes() {
		// TODO Auto-generated method stub
		return null;
	}

	@RequestMapping(value = "/cliente/cli_id", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Cliente>> acharcli_id(@PathVariable Integer cli_id) throws ParseException {
		List<Cliente> cliente = clienteService.entradascli_id(cli_id);
		if (cliente.isEmpty()) {
			System.out.println("Nenhum registro no banco de dados.");
			return new ResponseEntity<List<Cliente>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Cliente>>(cliente, HttpStatus.OK);
	}

	private List<Cliente> entradascli_id(Integer cli_id) {
		// TODO Auto-generated method stub
		return null;
	}
	@RequestMapping(value = "/cliente/adicionar", method = RequestMethod.POST)
	public ResponseEntity<Void> adicionar(@RequestBody Cliente cliente) {
		clienteService.salvarclientes(cliente);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}

	private void salvarclientes(Cliente cliente) {
		// TODO Auto-generated method stub
		
	}


}
