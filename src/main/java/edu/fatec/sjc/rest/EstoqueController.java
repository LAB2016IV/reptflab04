package edu.fatec.sjc.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import edu.fatec.sjc.entity.Estoque;
import edu.fatec.sjc.entity.Produto;
import edu.fatec.sjc.entity.repositories.EstoqueRepository;
import edu.fatec.sjc.service.EstoqueService;

public class EstoqueController {
	
	@Autowired
	EstoqueService estoqueService;

	@Autowired
	EstoqueRepository estoqueRepository;

	@RequestMapping(value = "/estoque/lista", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Estoque>> listar() {
		List<Estoque> lista = estoqueService.listarEstoque();
		if (lista.isEmpty()) {
			System.out.println("Nenhum registro no banco de dados.");
			return new ResponseEntity<List<Estoque>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Estoque>>(lista, HttpStatus.OK);
	}

	@RequestMapping(value = "/estoque/est_id", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Estoque> achar(@PathVariable Integer est_id) {
		Estoque estoque = estoqueService.umEstoque(est_id);
		if (estoque == null) {
			System.out.println("Nenhum registro no banco de dados");
			return new ResponseEntity<Estoque>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<Estoque>(estoque, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/estoque/produto", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Estoque> acharProduto(@PathVariable Produto produto) {
		Estoque estoque = estoqueService.estoqueProduto(produto);
		if (estoque == null) {
			System.out.println("Nenhum registro no banco de dados");
			return new ResponseEntity<Estoque>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<Estoque>(estoque, HttpStatus.OK);
	}

	@RequestMapping(value = "/estoque/add", method = RequestMethod.POST)
	public ResponseEntity<Void> adicionar(@RequestBody Estoque estoque) {
		estoqueService.salvarEstoque(estoque);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/estoque/add/QTDD", method = RequestMethod.POST)
	public ResponseEntity<Void> atualizar(@RequestBody Produto produto, @RequestBody Integer inteiro) {
		Integer quantidade = estoqueRepository.quantidade(produto) + inteiro;
		estoqueRepository.atualizaEstoque(quantidade, produto);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}
	

}
