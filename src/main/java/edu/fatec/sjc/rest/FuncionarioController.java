package edu.fatec.sjc.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import edu.fatec.sjc.entity.Funcionario;
import edu.fatec.sjc.entity.repositories.FuncionarioRepository;
import edu.fatec.sjc.service.FuncionarioService;

public class FuncionarioController {
	

	@Autowired
	FuncionarioService funcionarioService;

	@Autowired
	FuncionarioRepository funcionarioRepository;

	@RequestMapping(value = "/funcionario/lista", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Funcionario>> listar() {
		List<Funcionario> lista = funcionarioService.listarFuncionarios();
		if (lista.isEmpty()) {
			System.out.println("Nenhum registro no banco de dados.");
			return new ResponseEntity<List<Funcionario>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Funcionario>>(lista, HttpStatus.OK);
	}

	@RequestMapping(value = "/funcionario/fun_matricula", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Funcionario> achar(@PathVariable Integer fun_matricula) {
		Funcionario funcionario = funcionarioService.umFuncionario(fun_matricula);
		if (funcionario == null) {
			System.out.println("Nenhum registro no banco de dados");
			return new ResponseEntity<Funcionario>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<Funcionario>(funcionario, HttpStatus.OK);
	}

	@RequestMapping(value = "/funcionario/add", method = RequestMethod.POST)
	public ResponseEntity<Void> adicionar(@RequestBody Funcionario funcionario) {
		funcionarioService.salvarFuncionario(funcionario);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}


}
