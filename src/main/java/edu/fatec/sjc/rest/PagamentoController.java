package edu.fatec.sjc.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import edu.fatec.sjc.entity.Pagamento;
import edu.fatec.sjc.entity.repositories.PagamentoRepository;
import edu.fatec.sjc.service.PagamentoService;

public class PagamentoController {
	
	@Autowired
	PagamentoRepository pagamentoRepository;

	@Autowired
	PagamentoService pagamentoService;

	@RequestMapping(value = "/pagamento/listar", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Pagamento>> listar() {
		List<Pagamento> lista = pagamentoService.listarPagamentos();
		if (lista.isEmpty()) {
			System.out.println("Nenhum registro no banco de dados.");
			return new ResponseEntity<List<Pagamento>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Pagamento>>(lista, HttpStatus.OK);
	}

	@RequestMapping(value = "/cliente/cpf", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Pagamento> achar(@PathVariable Integer pag_id) {
		Pagamento pagamento = pagamentoService.umPagamento(pag_id);
		if (pagamento == null) {
			System.out.println("Nenhum registro no banco de dados");
			return new ResponseEntity<Pagamento>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<Pagamento>(pagamento, HttpStatus.OK);
	}

	@RequestMapping(value = "/cliente/add", method = RequestMethod.POST)
	public ResponseEntity<Void> adicionar(@RequestBody Pagamento pagamento) {
		pagamentoService.salvarPagamento(pagamento);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}


}
