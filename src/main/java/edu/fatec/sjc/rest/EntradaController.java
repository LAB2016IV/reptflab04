package edu.fatec.sjc.rest;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import edu.fatec.sjc.entity.Entrada;
import edu.fatec.sjc.entity.repositories.EntradaRepository;
import edu.fatec.sjc.service.EntradaService;

public class EntradaController {
	

	@Autowired
	EntradaService entradaService;

	@Autowired
	EntradaRepository entradaRepository;

	@RequestMapping(value = "/entrada/lista", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Entrada>> listar() {
		List<Entrada> lista = entradaService.listarEntradas();
		if (lista.isEmpty()) {
			System.out.println("Nenhum registro no banco de dados.");
			return new ResponseEntity<List<Entrada>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Entrada>>(lista, HttpStatus.OK);
	}

	@RequestMapping(value = "/entrada/ent_id", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Entrada>> acharent_id(@PathVariable Integer ent_id) throws ParseException {
		List<Entrada> entrada = entradaService.entradasEnt_id(ent_id);
		if (entrada.isEmpty()) {
			System.out.println("Nenhum registro no banco de dados.");
			return new ResponseEntity<List<Entrada>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Entrada>>(entrada, HttpStatus.OK);
	}

	@RequestMapping(value = "/entrada/fornecedor", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Entrada>> acharfornecedor(@PathVariable String fornecedor) {
		List<Entrada> entrada = entradaService.entradasFornecedor(fornecedor);
		if (entrada.isEmpty()) {
			System.out.println("Nenhum registro no banco de dados.");
			return new ResponseEntity<List<Entrada>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Entrada>>(entrada, HttpStatus.OK);
	}
	

	@RequestMapping(value = "/entrada/produto", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Entrada>> acharProduto(@PathVariable String produto) {
		List<Entrada> entrada = entradaService.entradasProduto(produto);
		if (entrada.isEmpty()) {
			System.out.println("Nenhum registro no banco de dados.");
			return new ResponseEntity<List<Entrada>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Entrada>>(entrada, HttpStatus.OK);
	}


	@RequestMapping(value = "/entrada/adicionar", method = RequestMethod.POST)
	public ResponseEntity<Void> adicionar(@RequestBody Entrada entrada) {
		entradaService.salvarEntradas(entrada);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}


}
