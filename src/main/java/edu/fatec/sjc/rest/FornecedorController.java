package edu.fatec.sjc.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import edu.fatec.sjc.entity.Fornecedor;
import edu.fatec.sjc.entity.repositories.FornecedorRepository;
import edu.fatec.sjc.service.FornecedorService;

public class FornecedorController {
	

	@Autowired
	FornecedorService fornecedorService;

	@Autowired
	FornecedorRepository fornecedorRepository;

	@RequestMapping(value = "/fornecedor/listar", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Fornecedor>> listar() {
		List<Fornecedor> lista = fornecedorService.listar();
		if (lista.isEmpty()) {
			System.out.println("Nenhum registro no banco de dados.");
			return new ResponseEntity<List<Fornecedor>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Fornecedor>>(lista, HttpStatus.OK);
	}

	@RequestMapping(value = "/fornecedor/codigo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Fornecedor> achar(@PathVariable Integer for_id) {
		Fornecedor fornecedor = fornecedorService.umFornecedor(for_id);
		if (fornecedor == null) {
			System.out.println("Nenhum registro no banco de dados");
			return new ResponseEntity<Fornecedor>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<Fornecedor>(fornecedor, HttpStatus.OK);
	}


}
