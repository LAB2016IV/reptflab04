package edu.fatec.sjc.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import edu.fatec.sjc.entity.Acarretamentos;
import edu.fatec.sjc.entity.repositories.AcarretamentosRepository;
import edu.fatec.sjc.service.AcarretamentosService;

public class AcarretamentosController {

	@Autowired
	AcarretamentosService acarretamentosService;

	@Autowired
	AcarretamentosRepository acarretamentosRespository;

	@RequestMapping(value = "/acarretamento/lista", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Acarretamentos>> listar() {
		List<Acarretamentos> lista = acarretamentosService.listarAcarretamentos();
		if (lista.isEmpty()) {
			System.out.println("Nenhum registro no banco de dados.");
			return new ResponseEntity<List<Acarretamentos>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Acarretamentos>>(lista, HttpStatus.OK);
	}

	@RequestMapping(value = "/acarretamento/aca_id", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Acarretamentos> achar(@PathVariable Integer aca_id) {
		Acarretamentos acarretamentos = acarretamentosService.umAcarretamentos(aca_id);
		if (acarretamentos == null) {
			System.out.println("Nenhum registro no banco de dados");
			return new ResponseEntity<Acarretamentos>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<Acarretamentos>(acarretamentos, HttpStatus.OK);
	}


}
