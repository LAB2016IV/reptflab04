package edu.fatec.sjc.test;

import java.sql.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import edu.fatec.sjc.entity.Acarretamentos;
import edu.fatec.sjc.entity.Cliente;
import edu.fatec.sjc.entity.Entrada;
import edu.fatec.sjc.entity.Estoque;
import edu.fatec.sjc.entity.Fornecedor;
import edu.fatec.sjc.entity.Funcionario;
import edu.fatec.sjc.entity.Pagamento;
import edu.fatec.sjc.entity.Produto;
import edu.fatec.sjc.entity.Tipo;
import edu.fatec.sjc.entity.Venda;

public class teste {
	private static EntityManager em;

	@Before
	public void beforeEach() {
		em = Persistence.createEntityManagerFactory("trabalho-01").createEntityManager();
	}
	
	
	
	@Test
	public void testEntityManagerGetInstance() {
		Assert.assertNotNull(em);
	}

	@Test
	public void testInsertVenda() {
		Cliente cli = new Cliente("Michele");
		Tipo tip = new Tipo("roupa");
		Pagamento pag = new Pagamento(1, "VISA");
		Fornecedor forn = new Fornecedor(123,"nike");	
		Produto pro = new Produto(90.0,"camisa", tip);
		Estoque est = new Estoque(50, pro);
		Entrada en = new Entrada(12.0, Date.valueOf("2016-07-16"), 15, forn, pro);
		Acarretamentos aca = new Acarretamentos(forn,"forn");
		Funcionario fun = new Funcionario("Jhon Doe", 4); 
		Venda ven = new Venda(2,Date.valueOf("2016-08-12"), pro, fun, cli, pag);
		
		EntityTransaction et = em.getTransaction();
		try {
			et.begin();
			em.persist(cli);
			em.persist(tip);
			em.persist(pag);
			em.persist(forn);
			em.persist(pro);
			em.persist(est);
			em.persist(aca);
			em.persist(en);
			em.persist(fun);
			em.persist(ven);
			et.commit();
		
			
		} catch (Exception e) {
			et.rollback();
			Assert.fail();
		} 
	}
	
	@After
	public void afterEach() {
		em.close();
	}
	
}
